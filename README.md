## README

This is the backend APIs for ecommerce system

#### features

- Admin
  - add new items to the system
  - display all the orders
- User
  - show all the items in the system
  - add items to the cart
  - place the order
  - display the personal orders
