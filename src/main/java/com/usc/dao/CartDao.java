package com.usc.dao;

import com.usc.beans.Cart;
import com.usc.beans.Item;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CartDao extends JpaRepository<Cart, Long> {
    @Override
    Optional<Cart> findById(Long aLong);
}
