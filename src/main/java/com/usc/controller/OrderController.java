package com.usc.controller;

import com.usc.beans.Order;
import com.usc.beans.User;
import com.usc.http.Response;
import com.usc.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController() // recept API request; default return json format; no need to add @ResponseBody
@RequestMapping("/orders")
public class OrderController {
    @Autowired
    OrderService orderService;

    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    @GetMapping
    public List<Order> showUserOrder(Authentication authentication) {
        return orderService.showUserOrder(authentication);
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping("/all")
    public Map<User, List<Order>> showAllOrder() {
        return orderService.showAllOrder();
    }

    @PreAuthorize("hasAuthority('ROLE_USER')")
    @PostMapping
    public Response placeOrder(Authentication authentication) {
        return orderService.placeOrder(authentication);
    }
}
