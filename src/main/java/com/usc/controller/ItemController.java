package com.usc.controller;

import com.usc.beans.Item;
import com.usc.http.Response;
import com.usc.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController() // recept API request; default return json format; no need to add @ResponseBody
@RequestMapping("/items")
public class ItemController {
    @Autowired
    ItemService itemService;

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping
    public List<Item> getAllItems() {
        return itemService.displayItem();
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @PostMapping
    public Response addItem(@RequestBody Item item) {
        return itemService.addItem(item);
    }
}
