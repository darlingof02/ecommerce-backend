package com.usc.controller;

import com.usc.beans.Cart;
import com.usc.beans.Item;
import com.usc.beans.User;
import com.usc.dao.UserDao;
import com.usc.http.Response;
import com.usc.service.CartService;
import com.usc.service.ItemService;
import com.usc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController() // recept API request; default return json format; no need to add @ResponseBody
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserDao userDao;

    @Autowired
    UserService userService;

    @Autowired
    ItemService itemService;

    @Autowired
    CartService cartService;

//    @Autowired
//    ItemService itemService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping
    public List<User> getusers() {
        return userDao.findAll();
    }

    @PostMapping
    public Response addUser(@RequestBody User user) { // json format parameter
        return userService.register(user);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    @PutMapping
    public Response changeUser(@RequestBody User user, Authentication authentication) {
        return userService.changePassword(user, authentication);
    }

//    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
//    @PostMapping
//    public Response addItem(@RequestBody Item item) {
//        return itemService.addItem(item);
//    }

    @DeleteMapping("/{id}")
    public Response deleteUser(@PathVariable int id) {
        return userService.deleteUser(id);
    }

    @GetMapping("/cart")
    public Cart showItemsInCart(Authentication authentication) {

        return userDao.findByUsername(authentication.getName()).getCart();
    }

    @PostMapping("/cart/{id}")
    public Response addItem(Authentication authentication, @PathVariable Long id) {
        return cartService.addItem(authentication, id);
    }
}
