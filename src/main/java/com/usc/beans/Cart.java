package com.usc.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "usc.cart")
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @JoinColumn(name = "user_id")
//    private User user;


    @OneToOne(mappedBy = "cart")
    @JsonIgnore
    private User user;

    @ManyToMany
    @JoinTable(name = "c_cart_c_item", joinColumns = {
            @JoinColumn(name = "cart_id", referencedColumnName = "id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "item_id", referencedColumnName = "id")
    })
    private List<Item> itemList = new ArrayList<>();

    public Cart() {

    }

    public Long getId() {
        return id;
    }

    //    public void setUser(User user) {
//        this.user = user;
//    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

//    public User getUser() {
//        return user;
//    }

    public List<Item> getItemList() {
        return itemList;
    }

    public Cart(Long id, List<Item> itemList) {
        this.id = id;
        this.itemList = itemList;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "id=" + id +
                ", itemList=" + itemList +
                '}';
    }

    public void setId(Long id) {
        this.id = id;
    }
}
