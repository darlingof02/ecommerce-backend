package com.usc.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "usc.order")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;


    @ManyToMany
    @JoinTable(name = "c_order_c_item", joinColumns = {
            @JoinColumn(name = "order_id", referencedColumnName = "id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "item_id", referencedColumnName = "id")
    })
    private List<Item> itemList = new ArrayList<>();

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;



    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public void setUser(User user) {
        this.user = user;
    }



    public List<Item> getItemList() {
        return itemList;
    }

    public Order() {

    }

    public User getUser() {
        return user;
    }

    public Order(Long id, long uid, List<Item> itemList, User user) {
        this.id = id;
        this.itemList = itemList;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
