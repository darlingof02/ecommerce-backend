package com.usc.beans;

import javax.persistence.*;

@Entity
@Table(name = "usc.item")
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "itemname", nullable = false)
    private String itemname;

    @Column(name = "price", nullable = false)
    private int price;

    @Column(name = "shopname", nullable = false)
    private String shopname;

//    @Column(name = "quantity", nullable = false)
//    private int quantity;

    public Item() {

    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", itemname='" + itemname + '\'' +
                ", price=" + price +
                ", shopname='" + shopname + '\'' +
                '}';
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    public String getItemname() {
        return itemname;
    }

    public int getPrice() {
        return price;
    }

    public String getShopname() {
        return shopname;
    }

    public Item(Long id, String itemname, int price, String shopname) {
        this.id = id;
        this.itemname = itemname;
        this.price = price;
        this.shopname = shopname;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
