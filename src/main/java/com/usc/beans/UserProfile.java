package com.usc.beans;


import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity // jpa or mapping
@Table(name = "usc_user_profile")
public class UserProfile implements GrantedAuthority {
    private static final long serialVersionUID = 1L;
    @Id
    private int id;

    public UserProfile() {
    }

    public UserProfile(String type) {
        this.type = type;
    }

    public UserProfile(int id) {
        this.id = id;
    }

    @Column
    private String type;

    public void setId(int id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "UserProfile{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public UserProfile(int id, String type) {
        this.id = id;
        this.type = type;
    }

    @Override
    public String getAuthority() {
        return type;
    }
}
