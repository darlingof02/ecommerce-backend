package com.usc.beans;


//import javax.persistence.*;

import javax.persistence.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity // JPA or mapping
@Table(name = "usc.user") // if no parameter, it will take class name as its table name
public class User implements UserDetails {
    private static final long serialVersionUID = 1L;
    @Id // primary key
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ")
    @SequenceGenerator(name = "SEQ", sequenceName = "CCGG_USER_SEQ")
    private int id;

    @Column(name = "username", unique = true, nullable = false) //define other field, if on name parameter, it will take variable name as its field
    private String username;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", cart=" + cart +
                ", profiles=" + profiles +
                ", orderList=" + orderList +
                '}';
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    public User(int id, String username, String password, Cart cart, List<UserProfile> profiles, List<Order> orderList) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.cart = cart;
        this.profiles = profiles;
        this.orderList = orderList;
    }

    public Cart getCart() {
        return cart;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    @Column(name = "password", nullable = false)
    private String password;

//    @OneToOne(mappedBy = "user",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    private Cart cart;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "cart_id")
    private Cart cart;

    @ManyToMany(fetch = FetchType.EAGER) // not lazy, it will create table in the first place
    @JoinTable(name = "c_user_c_user_profile", joinColumns = {
            @JoinColumn(name = "user_id", referencedColumnName = "id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "user_profile_id", referencedColumnName = "id")
    })
    private List<UserProfile> profiles = new ArrayList<>();


    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    List<Order> orderList = new ArrayList<>();




    public User() {
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return profiles;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    public User(int id, String username, String password, List<UserProfile> profiles) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.profiles = profiles;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setProfiles(List<UserProfile> profiles) {
        this.profiles = profiles;
    }

    public int getId() {
        return id;
    }

    public List<UserProfile> getProfiles() {
        return profiles;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }


}
