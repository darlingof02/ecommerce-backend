package com.usc.service;

import com.usc.beans.Item;
import com.usc.beans.Order;
import com.usc.beans.User;
import com.usc.dao.OrderDao;
import com.usc.http.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class OrderService {
    @Autowired
    CartService cartService;

    @Autowired
    UserService userService;

    @Autowired
    OrderDao orderDao;

    public Response placeOrder(Authentication authentication) {
        User user = userService.getUser(authentication);
        if (user == null)
            return new Response(false, "User does not exist");
        List<Item> itemList = cartService.showCart(user);
        Order order = new Order();
        order.setItemList(itemList);
        order.setUser(user);
        orderDao.save(order);
        cartService.emptyCart(user);
        return new Response(true);
    }

    public List<Order> showUserOrder(Authentication authentication) {
        User user = userService.getUser(authentication);
        if (user == null)
            return null;
        List<Order> orderList = user.getOrderList();
        return orderList;
    }

    public Map<User, List<Order>> showAllOrder() {
        List<Order> orderList = orderDao.findAll();
        Map<User, List<Order>> map = new HashMap<>();
        for (Order order: orderList) {
            map.putIfAbsent(order.getUser(), new ArrayList<>());
            map.get(order.getUser()).add(order);
        }
        return map;
    }
}
