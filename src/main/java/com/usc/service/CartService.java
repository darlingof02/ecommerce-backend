package com.usc.service;

import com.usc.beans.Cart;
import com.usc.beans.Item;
import com.usc.beans.User;
import com.usc.dao.CartDao;
import com.usc.dao.ItemDao;
import com.usc.dao.UserDao;
import com.usc.http.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CartService {
    @Autowired
    CartDao cartDao;

    @Autowired
    UserDao userDao;

    @Autowired
    ItemDao itemDao;

    public List<Item> showCart(User user) {
        return user.getCart().getItemList();
    }

    public Response addItem(Authentication authentication, Long itemId) {
        User user = userDao.findByUsername(authentication.getName());
        Optional<Item> ls = itemDao.findById(itemId);
        Item item;
        if (ls.isPresent()) {
            item = ls.get();
        }
        else {
            return new Response(false, "no item has Id" + itemId);
        }
        Cart cart = user.getCart();
        cart.getItemList().add(item);
        cartDao.save(cart);
        return new Response(true);
    }



    public void emptyCart(User user) {
        Cart cart = user.getCart();
        cart.setItemList(new ArrayList<>());
        cartDao.save(cart);
    }
//    List<Item> getItems(User user) {
//        Cart cart = cartService.
//    }
}
