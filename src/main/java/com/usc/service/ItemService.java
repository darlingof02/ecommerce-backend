package com.usc.service;

import com.usc.beans.Item;
import com.usc.dao.ItemDao;
import com.usc.http.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ItemService {
    @Autowired
    ItemDao itemDao;

    public Response addItem(Item item) {
        itemDao.save(item);
        System.out.println(item);
        return new Response(true);
    }

    public List<Item> displayItem() {
        return itemDao.findAll();
    }
}
